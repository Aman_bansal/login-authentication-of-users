package org.studyeasy.spring.hibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name ="logintest")
@Table(name="logintest")
public class Users {
	
	@Id
	@Column(name="USERID")
	int userId;
	
	@Column(name="USERNAME")
	String username;
	@Column(name="PASSWORD")
	String password;
	
	
	public Users(int userId, String username, String password) {
		super();
		this.userId = userId;
		this.username = username;
		this.password = password;
	}


	public Users()
	{
		
	}


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return "Users [userId=" + userId + ", username=" + username + ", password=" + password + "]";
	}
	

	
}

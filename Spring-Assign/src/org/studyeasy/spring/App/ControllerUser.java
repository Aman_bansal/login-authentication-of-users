package org.studyeasy.spring.App;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.studyeasy.spring.DAO.DaoImpl;
import org.studyeasy.spring.hibernate.entity.Users;

@Controller
public class ControllerUser {
	
	private static final Logger logger = Logger.getLogger(ControllerUser.class);
	DaoImpl daoimpl = new DaoImpl();
	@GetMapping("/")
	public ModelAndView home(Users  modelUser)
	{
		ModelAndView modelandview = new ModelAndView("home");
		modelandview.addObject("authenticate",modelUser);
		
		return modelandview;
		                                                                    
  }
	@RequestMapping("/login")
	public ModelAndView getUserInfo(@ModelAttribute("authenticate") Users modelUser)
	{

Users value=daoimpl.getInfo(modelUser);             

                if(value !=null)
                {
                	ModelAndView modelandview = new ModelAndView("Success");
            		return modelandview;                    
                	
                }
                else
                {
                	ModelAndView modelandview = new ModelAndView("redirect:/");
            		return modelandview;
                }
                
                                                                
  }
	@GetMapping("/admin")
	public ModelAndView getAdmin()
	{
		ModelAndView modelandview = new ModelAndView("admin");
		logger.warn("admin ur); is accessed by xyx ");
		return modelandview;
		                                                                    
  }

}
